var assert = require("chai").assert;
var expect = require("chai").expect;
var Candidate = require('../model/entretien/candidate');
var Recruiter = require('../model/entretien/recruiter');


describe('booking', () =>{
    it('Booking should be done', ()=> {
        let candidate = new Candidate(1,"Bob", {}, null, true, true);

        candidate.isBooking(candidate)
            .then( (user) => {
                assert.isNotNull(user.date);
            })
        });
        it('Booking should already be done', ()=> {
            let candidate = new Candidate(1,"Bob", {}, "date", true, true);

            candidate.isBooking(candidate)
                .then( (user) => {
                    assert.equal(err, "Candidate already booked at date");
                })
        });
        it('User should not exist', ()=> {
            let candidate = new Candidate();

            candidate.isBooking(candidate)
                .catch( (err) => {
                    assert.equal(err,"User not exist");
                })
        });
    });

describe('interested', () =>{
    it('should be interested', ()=> {
        let candidate = new Candidate(1,"Bob", {}, null, true, false);

        candidate.isInterested(candidate)
            .then( (user) => {
                assert.equal(user.interested , true);
            })
    });
    it('User should not exist', ()=> {
        let candidate = new Candidate();
        candidate.isInterested(candidate)
            .catch( (err) => {
                assert.equal(err,"User not exist");
            })
    });
    it('should already be interested', ()=> {
        let candidate = new Candidate(1,"Bob", {}, null, true, true);

        candidate.isInterested(candidate)
            .then( (err) => {
                assert.equal(err , "Candidate already interested");
            })
    });
});


describe('contacted', () =>{
    it('should be contacted', ()=> {
        let candidate = new Candidate(1,"Bob", {}, null, false, false);

        candidate.isContacted(candidate)
            .then( (user) => {
                assert.equal(user.contacted, true);
            })
    });
    it('User should not exist', ()=> {
        let candidate = new Candidate();

        candidate.isContacted(candidate)
            .catch( (err) => {
                assert.equal(err,"User not exist");
            })
    });
    it('should already be contacted', ()=> {
        let candidate = new Candidate(1,"Bob", {}, null, true, false);

        candidate.isContacted(candidate)
            .then( (err) => {
                assert.equal(err , "Candidate already contacted");
            })
    });
});


describe('canTest', () =>{
    it('should be able to test', ()=> {
        let r = new Recruiter(1, "Billy", true, false);

        r.test(r)
            .then( (user) => {
                assert.equal(user,r);
            })
    });
    it('should not be able to test', ()=> {
        let r = new Recruiter(1, "Billy", false, false);

        r.test(r)
            .catch( (err) => {
                assert.equal(err,"Recruiter can't test");
            })
    });
});


describe('isAvailable', () =>{
    it('should be available', ()=> {
        let r = new Recruiter(1, "Billy", true, true);
        r.isAvailable(r)
            .then( (user) => {
                assert.equal(user,r);
            })
    });
    it('should not be available', ()=> {
        let r = new Recruiter(1, "Billy", true, false);
        r.isAvailable(r)
            .catch( (err) => {
                assert.equal(err,"Recruiter is not available");
            })
    });
});