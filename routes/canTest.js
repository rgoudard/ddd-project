var express = require('express');
var router = express.Router();
var Recruiter = require('../model/entretien/recruiter');




router.get("/", function (req, res) {

    const json = require('../recruiter.json');

    let c = json.recruiter;
    let nbRecruiters = c.length;
    let recruitersCanTest = [];

    for (let i = 0; i < nbRecruiters ; i++){

        let recruiter = new Recruiter(c[i].id , c[i].name , c[i].canTest , c[i].available);
        recruiter.test(recruiter)
            .then( (user) => {
                recruitersCanTest.push(user);
            }).catch( (err) => {
                console.log(err);
            })
            .then((u) =>
            {
                res.send(recruitersCanTest);
            }).catch( (err) => {
                console.log("");
            });
    }
});


module.exports = router;
