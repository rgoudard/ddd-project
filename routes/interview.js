var express = require('express');
var router = express.Router();
var Interview = require('../model/entretien/interview');

router.get("/", function (req, res) {

    let interview = new Interview();
    interview.scheduleInterview(interview);

    interview.showInterview(interview);

});

module.exports = router;
