var express = require('express');
var router = express.Router();
var Recruiter = require('../model/entretien/recruiter');




router.get("/", function (req, res) {

    const json = require('../recruiter_canTest.json');

    let c = json.recruiter;
    let nbRecruitersCanTest = c.length;
    let recruitersIsAvailable = [];

    for (let i = 0; i < nbRecruitersCanTest ; i++){

        let recruiter = new Recruiter(c[i].id , c[i].name , c[i].canTest , c[i].available);
        recruiter.isAvailable(recruiter)
            .then( (user) => {
                recruitersIsAvailable.push(user);
            }).catch( (err) => {
            console.log(err);
        })
            .then((u) =>
            {
                res.send(recruitersIsAvailable);
            }).catch( (err) => {
        });
    }
});

module.exports = router;
