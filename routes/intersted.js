var express = require('express');
var router = express.Router();
var Candidate = require('../model/entretien/candidate');



router.get("/", function (req, res) {

    let candidate = new Candidate(1,"BONJOUR", {}, null , true, true);
    candidate.isInterested(candidate)
        .then( (user) => {
            res.send (user);
        })
        .catch( (err) => {
            res.send (err);
        })
});

module.exports = router;
