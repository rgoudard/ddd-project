var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');

let booking = require('./routes/booking');
let contacted = require('./routes/contacted');
let interested = require ( './routes/intersted');
let canTest = require ( './routes/canTest');
let isAvailable = require ( './routes/isAvailable');
let interview = require ( './routes/interview');


var app = express();

//app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/contacted',contacted);
app.use('/booking',booking);
app.use('/interested',interested);
app.use('/canTest',canTest);
app.use('/isAvailable',isAvailable);
app.use('/interview',interview);

module.exports = app;
