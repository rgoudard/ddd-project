'use strict';

var Schedule = require('./schedule');


class Candidate {
    constructor(id,name,cv,date,interested, contacted) {
        this.id = id;
        this.name = name;
        this.cv = cv ;
        this.date = date;
        this.interested = interested;
        this.contacted = contacted;

    }
}

Candidate.prototype.isContacted = function(candidate){

if ( candidate === undefined){
    return Promise.reject("User not exist");
}

if (candidate.contacted !== false ){
    return Promise.reject("Candidate already contacted");
}else{
    candidate.contacted = true;
    return Promise.resolve(candidate);
}
};

Candidate.prototype.isInterested = function (candidate){

    if ( candidate === undefined){
        return Promise.reject("User not exist");
    }

    if (candidate.interested !== false ){
        return Promise.reject("Candidate already interested");
    }else{
        candidate.interested = true;
        return Promise.resolve(candidate);
    }
};


Candidate.prototype.isBooking = function (candidate){

    if ( candidate === undefined){
        return Promise.reject("User not exist");
    }

    if (candidate.date !== null ){
        return Promise.reject("Candidate already booked at " + candidate.date);
    }else{
        candidate.date = new Schedule("22/03/2019" , "13:30" , "15:30");
        return Promise.resolve(candidate);
    }
};

module.exports = Candidate;




