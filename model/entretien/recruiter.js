'use strict';

class Recruiter {
    constructor(id , name , canTest , available) {

        this.id = id;
        this.name = name;
        this. canTest = canTest;
        this.available = available;

    }
}

Recruiter.prototype.test = function (recruiter){

    if ( recruiter.canTest === true ){
        return Promise.resolve(recruiter);
    }else {
        return Promise.reject("Recruiter can't test");
    }
};

Recruiter.prototype.isAvailable = function(recruiter){

    if ( recruiter.available === true ){
        return Promise.resolve(recruiter);
    }else {
        return Promise.reject("Recruiter is not available");
    }

};

module.exports = Recruiter;





