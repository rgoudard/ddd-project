'use strict';

var Candidate = require('./candidate');
var Recruiter = require('./recruiter');
var Schedule = require('./schedule');




class Interview {
    constructor( candidate , recruiter , schedule ) {

        this.candidate = candidate ;
        this.recruiter = recruiter;
        this.schedule = schedule;
    }
}

Interview.prototype.showInterview = function(interview) {

    if (interview.candidate !== undefined && interview.recruiter !== undefined && interview.schedule !== undefined) {
        return Promise.resolve(interview);
    } else {
        return Promise.reject("The interview is not valid");
    }
};

Interview.prototype.scheduleInterview = function(){
    let c = new Candidate(1 , "Bonsoir", {} , null ,false, false);
    c.isContacted(c);
    c.isInterested(c);
    c.isBooking(c);

    let schedule = c.date;

    let recruiter = {};

    return new Interview(c,schedule,recruiter);



};


module.exports = Interview;